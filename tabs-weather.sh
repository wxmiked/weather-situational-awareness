#!/bin/sh
cat << DONE > public/tabs-weather.json
{
  "settingsReloadIntervalMinutes": 5,
  "fullscreen": true,
  "autoStart": true,
  "lazyLoadTabs": true,
  "websites": [
    {
      "url": "https://www.wpc.ncep.noaa.gov/noaa/noaa.gif",
      "duration": 30,
      "tabReloadIntervalSeconds": 3000
    },
    {
      "url": "https://rapidrefresh.noaa.gov/hrrr/HRRRsmoke/for_web/hrrr_ncep_jet/$(date +%Y%m%d%H -d '-2 hours')/full/trc1_full_sfc_f002.png",
      "duration": 60,
      "tabReloadIntervalSeconds": 900
    },
    {
      "url": "https://maps.cocorahs.org/?maptype=precip&units=us&base=std&cp=BluYlwRed&datetype=daily&displayna=0&date=$(date -I -d '-14 hours')&key=dynamic&overlays=state,county&bbox=-139.61425781250003,18.93746442964186,-55.23925781250001,53.82659674299413",
      "duration": 30,
      "tabReloadIntervalSeconds": 7200
    },
    {
      "url": "https://www.purpleair.com/map?opt=1/mAQI/a10/cC0#10/37.944/-120.6423",
      "duration": 60,
      "tabReloadIntervalSeconds": 600
    },
    {
      "url": "https://firms.modaps.eosdis.nasa.gov/usfs/map/#d:$(date -I -d '-1 day')..$(date -I);l:noaa20-viirs,viirs,modis_a,modis_t,active-usa,active-ca,country-outline,noaa-red-flag,noaa-fire-watch,fire-perimeter,forecast-usa;@-108.5,39.9,6z",
      "duration": 60,
      "tabReloadIntervalSeconds": 3600
    },
    {
      "url": "https://rapidrefresh.noaa.gov/hrrr/HRRRsmoke/for_web/hrrr_ncep_jet/$(date +%Y%m%d%H -d '-2 hours')/SW/trc1_SW_sfc_f002.png",
      "duration": 60,
      "tabReloadIntervalSeconds": 300
    },
    {
      "url": "https://www.arcgis.com/apps/webappviewer/index.html?id=6dc469279760492d802c7ba6db45ff0e&extent=-14458741.1718%2C4373568.4587%2C-13020502.0475%2C5246785.0699%2C102100&showLayers=mapNotes_7061%3BmapNotes_7061_3%3BmapNotes_7061_2%3BmapNotes_7061_1%3BmapNotes_7061_0%3Bsurvey123_bb6eda36d0804180a7546ca7ebf0c62b_stakeholder_7746%3BUSA_Wildfires_v1_6433%3BNOAA_METAR_current_wind_speed_direction_v1_6979%3BMODIS_Thermal_v1_2349%3BSatellite_VIIRS_Thermal_Hotspots_and_Fire_Activity_3355%3BNWS_Watches_Warnings_v1_7134%3BUSA_Parks_7234%3BUSA_Counties_Generalized_4172%3BUSA_Counties_9862%3Bkml_3257_folder_0%3Bkml_3257_folder_1",
      "duration": 30,
      "tabReloadIntervalSeconds": 300
    },
    {
      "url": "https://alerts.weather.gov/cap/wwaatmget.php?x=CAC009&y=1",
      "duration": 30,
      "tabReloadIntervalSeconds": 300
    },
    {
      "url": "http://beta.alertwildfire.org/tile-display/viewer/?cams=Axis-FowlerPeak&cams=Axis-UpperBear",
      "duration": 30,
      "tabReloadIntervalSeconds": 1200
    },
    {
      "url": "https://forecast.weather.gov/MapClick.php?lon=-120.65149783913512&lat=38.06264649835049#.YvFWV2iYVhF",
      "duration": 60,
      "tabReloadIntervalSeconds": 1200
    }
  ]
}
DONE
